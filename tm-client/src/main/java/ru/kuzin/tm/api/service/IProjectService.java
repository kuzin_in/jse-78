package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.ProjectDTO;
import ru.kuzin.tm.enumerated.Status;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @NotNull
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name);

}